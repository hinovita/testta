/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testta;

import cern.colt.matrix.DoubleMatrix2D;
import org.apache.commons.math3.linear.MatrixUtils;
import cern.colt.matrix.impl.DenseDoubleMatrix2D;
import cern.colt.matrix.linalg.SingularValueDecomposition;
import java.util.Arrays;
import pembobotan.Pembobotan;
import pembobotan.ReduksiDimensi;
import preprocessing.Stemming;
import preprocessing.StopWordList;

/**
 *
 * @author User-pc
 */
public class TestTA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Pembobotan bobot = new Pembobotan("D:\\salenati");
        bobot.doPembobotan();
        double[][] data = bobot.hasilPembobotan;
        for(int i=0; i<data.length; i++){
            System.out.println(bobot.getGlobalTermList().getTermAt(i).getTerm() + " " + Arrays.toString(data[i]));
        }

        DoubleMatrix2D double2ddata = new DenseDoubleMatrix2D(data);
        System.out.println("\n");
        SingularValueDecomposition svd = new SingularValueDecomposition(double2ddata);
        System.out.println(svd.getU());
        System.out.println(svd.getS());
        System.out.println(svd.getV().viewDice());
        
        System.out.println("\n");
        System.out.println("U * S:");
        double[][] US = new double[svd.getU().rows()][svd.getS().columns()];
        for(int i=0; i<svd.getU().rows(); i++){
            System.out.print(bobot.getGlobalTermList().getTermAt(i).getTerm() + " : ");
            for(int j=0; j<svd.getS().columns(); j++){
                for(int k=0; k<svd.getU().columns(); k++){
                    US[i][j] += svd.getU().get(i, k) * svd.getS().get(k, j);
                }System.out.print(US[i][j] + " ");
            }System.out.print("\n");
        }
        
        System.out.println("\n");
        ReduksiDimensi reduksi = new ReduksiDimensi(bobot.getGlobalTermList(), bobot.getListDocument(), US);
        reduksi.doReduksiDimensi();
        for (double[] reducedMatrix : reduksi.getReducedMatrix()) {
            System.out.println(Arrays.toString(reducedMatrix));
        }

        System.out.println("\nS * V:");
        double[][] SV = new double[svd.getS().rows()][svd.getV().viewDice().columns()];
        for(int i=0; i<svd.getS().rows(); i++){
            for(int j=0; j<svd.getV().viewDice().columns(); j++){
                for(int k=0; k<svd.getS().columns(); k++){
                    SV[i][j] += svd.getS().get(i, k) * svd.getV().viewDice().get(k, j);
                }System.out.print(SV[i][j] + "  ");
            }System.out.print("\n");
        }

        System.out.println("--------------transpose SV----------------");
               double[][] transSV = new double[SV[0].length][SV.length];
               for(int rows = 0; rows < SV.length; rows++){
                       for(int cols = 0; cols < SV[0].length; cols++){
                               transSV[cols][rows] = SV[rows][cols];
                       }
               }
               for(double[] i:transSV){//2D arrays are arrays of arrays
                       System.out.println(Arrays.toString(i));
               }

        DoubleMatrix2D double2dred = new DenseDoubleMatrix2D(reduksi.getReducedMatrix());
        DoubleMatrix2D double2dtry = new DenseDoubleMatrix2D(transSV);
        KMedoids kmedoids = new KMedoids(double2dred, double2dtry, 5);
        kmedoids.cluster();
        System.out.println(kmedoids.getMedoids());
        System.out.println(kmedoids.getPartition());
    }
    
}
