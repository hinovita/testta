/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testta;

import cern.colt.matrix.DoubleMatrix1D;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.Instance;
import net.sf.javaml.distance.AbstractDistance;
/**
 *
 * @author User-pc
 */
public class CosineDistance {

    public double calculateDistance(DoubleMatrix1D x, DoubleMatrix1D y) {
        if (x.size() != y.size()) {
            throw new RuntimeException("Both instances should contain the same number of values.");
        }
        return 1-new CosineSimilarity().calculateDistance(x, y);

    }

    public double getMaximumDistance(Dataset data) {
        //TODO implement
        throw new RuntimeException("Method getMaximumDistance is not implemented in CosineDistance.");
    }
    
    public double getMinimumDistance(Dataset data) {
        // TODO implement
        throw new RuntimeException("Method getMinimumDistance is not implemented in CosineDistance.");
    }
}
