Penyakit stroke merupakan salah satu penyebab kematian manusia. Jumlah penderita Stroke
menunjukkan peningkatan setiap tahunnya. Hal ini terjadi karena kurangnya pengetahuan masyarakat akan
faktor resiko yang menyebabkan stroke. Terdapat banyak faktor resiko yang dapat mengarah pada stroke seperti
umur, jenis kelamin, penyakit diabetes, penyakit darah tinggi.
Salah satu teknik dalam mendiagnosis penyakit stroke ini adalah sistem pakar, dimana sistem pakar ini
dapat mendiagnosis penyakit stroke dengan meniru dari kerja para ahli/atau pakar. Sistem pakar untuk
mendiagnosa penyakit stroke ini menjadikan proses pelacakan untuk menentukan penyakit stroke dengan
inferensi Forward Chaining sebagai subjek penelitian. Sedangkan tingkat kepastiannya ditentukan
menggunakan metode Fuzzy dengan cara menghitung nilai probabilitas suatu penyakit dan membandingkan
probabilitas setiap gejalanya sehingga dapat diketahui tingkat keparahan penyakit stroke.
Skripsi ini bertujuan untuk menerapkan sistem pakar dalam mendiagnosis penyakit stroke dan membuat
aplikasi program yang mensimulasikan sistem pakar tersebut dengan menggunakan bahasa pemrograman
Microsoft Visual Basic 2008 dengan sistem operasi yang mendukung. Data-data yang didapat berupa gejalagejala
stroke yang diderita oleh user. Data-data tersebut diolah dengan sistem pakar berbasis aturan dan fakta
melalui mekanisme inferensi. Hasil pengolahan data tersebut berupa kesimpulan mengenai potensi penyakit
stroke.
Sistem Pakar, Forward Chaining, Metode Fuzzy.