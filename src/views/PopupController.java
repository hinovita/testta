package views;


import cern.colt.matrix.DoubleMatrix2D;
import views.PusatCluster;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WIN8
 */
public class PopupController {
    
    public void showPusatCluster(DoubleMatrix2D pusat){
        PusatCluster pusatCluster = new PusatCluster();
        pusatCluster.populatePusatCluster(pusat);
        pusatCluster.setVisible(true);
        pusatCluster.setLocationRelativeTo(null);
    }
    
    public void showPusatCluster(String[][] pusat){
        PusatCluster pusatCluster = new PusatCluster();
        pusatCluster.populatePusatCluster(pusat);
        pusatCluster.setVisible(true);
        pusatCluster.setLocationRelativeTo(null);
    }
    }

