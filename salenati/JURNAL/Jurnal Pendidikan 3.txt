Petunjuk melakukan sesuatu merupakan panduan bagi seseorang untuk melakukan sesuatu
yang diinginkan. Dalam menulis petunjuk harus memperhatikan urutan yang benar dan
menggunakan kalimat efektif. Tujuannya agar calon pembuat, pemakai suatu barang dapat
mengikuti langkah�langkah dalam petunjuk.
Penelitian ini bertujuan untuk mengetahui kemahiran menulis petunjuk melakukan sesuatu
dengan metode demonstrasi Siswa Kelas VIII Madrasah Tsanawiyah Negeri Bintan Timur
Tahun Pelajaran 2013/2014.
Penelitian ini menggunakan metode deskriptif kuantitatif. Populasi adalah siswa
kelas VIII berjumlah 141 siswa dan sampel berjumlah 40 siswa yang diambil 35% dari
populasi. Untuk mendapatkan data dalam skripsi, peneliti menggunakan tes essay dengan tema
cara membuat kotak pensil dari kardus bekas.
Hasil penelitian kemahiran menulis petunjuk melakukan sesuatu nilai rata-rata yang
diperoleh siswa dari seluruh aspek adalah 72,08. Dari hasil analisis data yang diperoleh bahwa
kemahiran menulis siswa termasuk dalam kriteria baik. Dan hipotesis sebelum penelitian ini
dilakukan ditolak.
 Kemahiran menulis, Petunjuk melakukan sesuatu